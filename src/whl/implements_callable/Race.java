package whl.implements_callable;

import java.util.concurrent.Callable;

public class Race implements Callable<Integer> {

	private String name;	//	名称
	private long time;		//延时
	private boolean flag=true;
	private int step=0;	//第几步
	
	public Race() {

	}
	public Race(String name) {
		this.name=name;
	}
	public Race(String name, long time) {
		this.name=name;
		this.time=time;
	}
	
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}
	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}
	/**
	 * @return the flag
	 */
	public boolean isFlag() {
		return flag;
	}
	/**
	 * @param flag the flag to set
	 */
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	/**
	 * @return the step
	 */
	public int getStep() {
		return step;
	}
	/**
	 * @param step the step to set
	 */
	public void setStep(int step) {
		this.step = step;
	}
	
	@Override
	public Integer call() throws Exception {

		while(flag) {
			Thread.sleep(time);
			step++;
		}
		
		return step;
	}

}
