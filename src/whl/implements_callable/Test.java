package whl.implements_callable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Test {

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		ExecutorService exser=Executors.newFixedThreadPool(2);
		Race rabbit=new Race("����", 500);
		Race tortoise=new Race("�ڹ�", 1000);

		Future<Integer> res1=exser.submit(rabbit);
		Future<Integer> res2=exser.submit(tortoise);
		
		Thread.sleep(2000);	
		rabbit.setFlag(false);
		tortoise.setFlag(false);
		
		int num1=res1.get();
		int num2=res2.get();
		
		System.out.println("�������ˣ�"+num1+"  "+"�ڹ����ˣ�"+num2);

		exser.shutdown();
	}

}
