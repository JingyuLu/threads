package whl.extends_thread;

public class Test {

	public static void main(String[] args) {

		Rabbit rab = new Rabbit();
		Tortoise tor = new Tortoise();

		rab.start();
		tor.start();
	}

}
