package whl.runnable_producer_vs_consumer_synchronized;

public class StorageSingle {

	private int productNums=0;
	private boolean flag=false;
	
	public synchronized void produce() {
		if(flag) {
			try {
				wait();	
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		productNums++;	
		System.out.println(Thread.currentThread().getName()+" 生产者：++++++++++"+productNums);
		flag=true;	
		notify();	
	}
	
	public synchronized void consume() {
		if(!flag) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println(Thread.currentThread().getName()+" 消费者：------"+productNums);
		flag=false;
		notify();
	}
}
