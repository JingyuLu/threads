package whl.runnable_producer_vs_consumer_synchronized;

public class ConsumerSingle implements Runnable{
	
	private StorageSingle st;
	
	public ConsumerSingle(StorageSingle st) {
		this.st=st;
	}

	@Override
	public void run() {

		while(true) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			st.consume();
		}
	}
}
