package whl.runnable_producer_vs_consumer_synchronized;

public class ConsumerMulti implements Runnable{
	
	private StorageMulti st;
	
	public ConsumerMulti(StorageMulti st) {
		this.st=st;
	}

	@Override
	public void run() {

		while(true) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			st.consume();
		}
	}
}
