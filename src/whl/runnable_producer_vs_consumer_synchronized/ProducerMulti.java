package whl.runnable_producer_vs_consumer_synchronized;

public class ProducerMulti implements Runnable{
	
	private StorageMulti st;
	
	public ProducerMulti(StorageMulti st) {
		this.st=st;
	}

	@Override
	public void run() {

		while(true) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			st.produce();
		}
	}
}
