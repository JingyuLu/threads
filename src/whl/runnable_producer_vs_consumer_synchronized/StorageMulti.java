package whl.runnable_producer_vs_consumer_synchronized;

public class StorageMulti {

	private int productNums=0;
	private boolean flag=false;
	
	public synchronized void produce() {
		while(flag) {	
			try {
				wait();	
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		productNums++;	
		System.out.println(Thread.currentThread().getName()+" 生产者：++++++++++"+productNums);
		flag=true;	
	}
	
	public synchronized void consume() {
		while(!flag) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println(Thread.currentThread().getName()+" 消费者：------"+productNums);
		flag=false;
		notifyAll();
	}
}
