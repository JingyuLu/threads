package whl.runnable_producer_vs_consumer_synchronized;

public class TestSingle {

	public static void main(String[] args) {

		StorageSingle storage=new StorageSingle();
		ProducerSingle p=new ProducerSingle(storage);
		ConsumerSingle c=new ConsumerSingle(storage);
		
		new Thread(p).start();
		new Thread(c).start();
	}

}
