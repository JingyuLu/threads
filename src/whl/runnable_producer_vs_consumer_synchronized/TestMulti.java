package whl.runnable_producer_vs_consumer_synchronized;

public class TestMulti {

	public static void main(String[] args) {

		StorageMulti storage=new StorageMulti();
		ProducerMulti p1=new ProducerMulti(storage);
		ProducerMulti p2=new ProducerMulti(storage);
		ConsumerMulti c1=new ConsumerMulti(storage);
		ConsumerMulti c2=new ConsumerMulti(storage);
		
		new Thread(p1).start();
		new Thread(p2).start();
		new Thread(c1).start();
		new Thread(c2).start();
		
	}

}
