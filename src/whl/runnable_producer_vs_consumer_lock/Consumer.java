package whl.runnable_producer_vs_consumer_lock;

public class Consumer implements Runnable{
	
	private Storage st;
	
	public Consumer(Storage st) {
		this.st=st;
	}

	@Override
	public void run() {

		while(true) {
			try {
				Thread.sleep(10);
				st.consume();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
