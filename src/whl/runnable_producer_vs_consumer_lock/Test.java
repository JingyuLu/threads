package whl.runnable_producer_vs_consumer_lock;

public class Test {

	public static void main(String[] args) {

		Storage storage=new Storage();
		Producer p1=new Producer(storage);
		Producer p2=new Producer(storage);
		Consumer c1=new Consumer(storage);
		Consumer c2=new Consumer(storage);
		
		new Thread(p1).start();
		new Thread(p2).start();
		new Thread(c1).start();
		new Thread(c2).start();

	}

}
