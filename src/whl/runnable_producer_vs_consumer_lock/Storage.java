package whl.runnable_producer_vs_consumer_lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Storage {

	private int productNums=0;
	private boolean flag=false;
	
	private Lock lock=new ReentrantLock();
	private Condition condition_producer=lock.newCondition();
	private Condition condition_consumer=lock.newCondition();


	public void produce() throws InterruptedException {
		try {
			lock.lock();
			while(flag) {
				condition_producer.await();
			}
			productNums++;	
			System.out.println(Thread.currentThread().getName()+" 生产者：++++++++++"+productNums);
			flag=true;
			condition_consumer.signal();
		}finally {
			lock.unlock();
		}
	}
	
	public void consume() throws InterruptedException {
		try {
			lock.lock();
			while(!flag) {
				condition_consumer.await();
			}
			System.out.println(Thread.currentThread().getName()+" 消费者：------"+productNums);
			flag=false;	
			condition_producer.signal();	
		}finally {
			lock.unlock();
		}
	}
}
