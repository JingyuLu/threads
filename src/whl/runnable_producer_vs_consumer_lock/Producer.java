package whl.runnable_producer_vs_consumer_lock;

public class Producer implements Runnable{
	
	private Storage st;
	
	public Producer(Storage st) {
		this.st=st;
	}

	@Override
	public void run() {

		while(true) {
			try {
				Thread.sleep(10);
				st.produce();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
