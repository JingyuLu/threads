package whl.singleton;

public class SingletonSingleThreadTest{

	public static void main(String[] args){
		
		//eager
//		SingletonEager se=new SingletonEager(); wrong, cannot find SingletonEager()option
		SingletonEager se1=SingletonEager.getInstance();
		SingletonEager se2=SingletonEager.getInstance();
		
		System.out.println("se1 address in memory: "+se1.hashCode());
		System.out.println("se2 address in memory: "+se1.hashCode());
		System.out.println("result: "+se1.equals(se2));
		
		System.out.println("****************************");
		
		//lazy
		SingletonLazy sl1=SingletonLazy.getInstance();
		SingletonLazy sl2=SingletonLazy.getInstance();
		
		System.out.println("sl1 address in memory: "+sl1.hashCode());
		System.out.println("sl2 address in memory: "+sl2.hashCode());
		System.out.println("result: "+sl1.equals(sl2));
		System.out.println(sl1.hashCode());
		System.out.println(sl2.hashCode());
		
		
	}
}
