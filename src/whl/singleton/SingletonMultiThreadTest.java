package whl.singleton;

/**
 * @author kings
 *
 */
public class SingletonMultiThreadTest extends Thread{

	@Override
	public void run() {
		for(int i=0;i<66;i++) {
			//eager
//			System.out.println("Current Thread: "+currentThread().getName()+" "+"eager address: "+SingletonEager.getInstance().hashCode());
			
			//lazy
			System.out.println("Current Thread: "+currentThread().getName()+" "+"lazy address: "+SingletonLazy.getInstance().hashCode());
		}
	}

	//test
	public static void main(String[] args){
		
		/*
		 * multiple threads
		 */
		SingletonMultiThreadTest sm1=new SingletonMultiThreadTest();
		SingletonMultiThreadTest sm2=new SingletonMultiThreadTest();
		
		sm1.start();
		sm2.start();
		
	}
}
