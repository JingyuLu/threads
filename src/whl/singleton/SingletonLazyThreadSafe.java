package whl.singleton;

public class SingletonLazyThreadSafe {

	private SingletonLazyThreadSafe() {
		
	}

	private static SingletonLazyThreadSafe singleton=null;
	
	public static SingletonLazyThreadSafe getInstance() {
		if(singleton==null) {
			synchronized(SingletonLazyThreadSafe.class) {
				if(singleton==null) {
					singleton=new SingletonLazyThreadSafe();
				}
			}
		}
		return singleton;
	}
	
}
