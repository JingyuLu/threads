package whl.singleton;

public class SingletonLazy {

	private SingletonLazy() {
		
	}
	
	private static SingletonLazy singleton=null;
	
	public static SingletonLazy getInstance() {
		if(singleton==null) {
			singleton=new SingletonLazy();
		}
		return singleton;
	}
	
}
