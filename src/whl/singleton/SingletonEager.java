package whl.singleton;

public class SingletonEager {

	/*
	 * 单例模式，特点：
	 * 单例类只能有一个实例
	 * 单例类必须自己创建自己的实例
	 * 单例类必须提供外界获取这个实例的方法
	 */
	/*
	 * 单例模式之饿汉模式
	 */
	//构造器私有化,则外界不能创建该类的实例
	private SingletonEager() {
		
	}
	
	/*
	 * 单例类必须自己创建自己的实例，
	 * 不能允许在类的外部修改内部创建的实例，
	 * 所以将这个实例用 private 声明。
	 * 为了外界能访问到这个实例，
	 * 我们还必须提供 get 方法得到这个实例。
	 * 因为外界不能 new 这个类，所以我们必须用 static 
	 * 来修饰字段和方法
	 */
	/*
	 * 1.饿汉式，静态修饰所以，类装载的时候就进行加载，而其后的new对Singleton对象进行了实例化，
	 * 即不管是否调用getInstance() Singleton的对象已经被创建出来。
	 * 这样避免了多线程的同步问题，多线程模式下线程安全（因为不管几个线程同时访问getInstance()方法，只有一个在类加载的时候被创建的实例）。
	 * 2.然而因实例化对象则延长加载时间和浪费内存（如果不使用）
	 */
	private static SingletonEager singleton=new SingletonEager();
	
	//提供get 方法以供外界获取单例
	/*
	 * singleton 外界不能用new来获取实例所以要用static修饰getInstance()来实现 class.getInstance()
	 */
	public static SingletonEager getInstance() {
		return singleton;
	}
	
}
