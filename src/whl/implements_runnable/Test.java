package whl.implements_runnable;


public class Test {

	public static void main(String[] args) {

		Rabbit rab=new Rabbit();
		Tortoise tor=new Tortoise();

		Thread staticProxy1 = new Thread(rab);
		Thread staticProxy2 = new Thread(tor);

		staticProxy1.start();
		staticProxy2.start();
	}

}
